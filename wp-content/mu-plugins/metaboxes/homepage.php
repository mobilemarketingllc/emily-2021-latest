<?php

// Homepage: Featured Content
add_action('cmb2_init', 'featured_content_metaboxes');
function featured_content_metaboxes() {
    $prefix = 'emh_';
    $cmb_group = new_cmb2_box(array(
        'id' => $prefix . 'featured_content',
        'title' => __('Featured Content', 'storefront'),
        'object_types' => array('page'),
        'show_on' => array('key' => 'id', 'value' => array(26)),
        'closed' => false
    ));

    $group_field_id = $cmb_group->add_field(array(
        'id' => $prefix . 'featured_content_slides',
        'type' => 'group',
        'description' => __('Add, edit, remove, or rearrange featured content slides.', 'storefront'),
        'options' => array(
            'group_title' => __('Slide {#}', 'storefront'),
            'add_button' => __('Add new slide', 'storefront'),
            'remove_button' => __('Remove slide', 'storefront'),
            'sortable' => true
        )
    ));

    $cmb_group->add_group_field($group_field_id, array(
        'id' => 'slide_background',
        'name' => __('Image', 'storefront'),
        'description' => __('Suggested size: 1200 x 440 pixels.', 'storefront'),
        'type' => 'file'
    ));

    $cmb_group->add_group_field($group_field_id, array(
        'id' => 'slide_headline',
        'name' => __('Headline', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_group_field($group_field_id, array(
        'id' => 'slide_subheadline',
        'name' => __('Sub-headline', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_group_field($group_field_id, array(
        'id' => 'slide_cta',
        'name' => __('CTA Text', 'storefront'),
        'description' => __('The text that goes on the call to action button.', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_group_field($group_field_id, array(
        'id' => 'slide_cta_url',
        'name' => __('CTA URL', 'storefront'),
        'description' => __('The URL that the CTA link takes the user to.', 'storefront'),
        'type' => 'text_url'
    ));
}

// Homepage: Featured Products area
add_action('cmb2_init', 'featured_products_area_metabox');
function featured_products_area_metabox() {
    $prefix = 'emh_';
    $cmb_group = new_cmb2_box(array(
        'id' => $prefix . 'featured_products',
        'title' => __('Featured Products', 'storefront'),
        'object_types' => array('page'),
        'show_on' => array('key' => 'id', 'value' => array(26)),
        'closed' => false
    ));

    $cmb_group->add_field(array(
        'id' => $prefix . 'featured_products_headline',
        'name' => __('Headline', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_field(array(
        'id' => $prefix . 'featured_products_desc',
        'name' => __('Description', 'storefront'),
        'type' => 'wysiwyg'
    ));
}