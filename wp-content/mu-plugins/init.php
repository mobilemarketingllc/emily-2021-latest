<?php

require WPMU_PLUGIN_DIR . "/metaboxes/global-theme-options.php";
require WPMU_PLUGIN_DIR . "/metaboxes/homepage.php";
require WPMU_PLUGIN_DIR . "/metaboxes/admin-featured-products.php";
require WPMU_PLUGIN_DIR . "/metaboxes/flooring.php";
require WPMU_PLUGIN_DIR . "/metaboxes/seo.php";

// CMB2
add_action('init', 'cmb_initialize_metaboxes');
function cmb_initialize_metaboxes() {
    require_once 'lib/cmb2/init.php';
}