var gulp = require('gulp');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var del = require('del');
var rev = require('gulp-rev');
var revDel = require('rev-del');
var livereload = require('gulp-livereload');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');

var config = {
    paths: {
        headerScripts: [
        ],
        footerScripts: [
            'bower_components/swiper/dist/js/swiper.jquery.min.js',
            'assets/js/jquery-ui.min.js',
            'assets/js/site.js'
        ],
        minOnlyScripts: [
            '../../plugins/daves-wordpress-live-search/js/daves-wordpress-live-search.js'
        ],
        styles: [
            'style.css',
            'bower_components/swiper/dist/css/swiper.min.css',
            'assets/sass/jquery-ui.min.css',
            'assets/sass/variables.scss',
            'assets/sass/mixins.scss',
            'assets/sass/main.scss'
        ]
    }
};
config.paths.scripts = []
    .concat(config.paths.headerScripts)
    .concat(config.paths.footerScripts);

gulp.task('lint', function() {
    return gulp.src(config.paths.scripts)
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(jshint({
            multistr: true
        }))
        .pipe(jshint.reporter('default'));
});

gulp.task('styles', function() {
    return gulp.src(config.paths.styles)
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('styles.scss'))
        .pipe(sass())
        .pipe(postcss([autoprefixer({browsers: ['last 2 versions']})]))
        .pipe(rename('styles.min.css'))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('assets/public/css'));
});

gulp.task('scripts:minOnly', function() {
    // Minify in place
    return gulp.src(config.paths.minOnlyScripts, {
            base: "./"
        })
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(uglify())
        .pipe(gulp.dest("."))
});

gulp.task('scripts:header', function() {
    return gulp.src(config.paths.headerScripts)
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('header.js'))
        .pipe(gulp.dest('assets/public/js'))
        .pipe(rename('header.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('assets/public/js'));
});

gulp.task('scripts:footer', function() {
    return gulp.src(config.paths.footerScripts)
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('footer.js'))
        .pipe(gulp.dest('assets/public/js'))
        .pipe(rename('footer.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('assets/public/js'));
});

gulp.task('clean', function() {
    return del('assets/public');
});

gulp.task('version', function() {
    gulp.src([
        'assets/public/css/styles.min.css',
        'assets/public/js/header.min.js',
        'assets/public/js/footer.min.js'
    ])
        .pipe(plumber(function(error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(gulp.dest('assets/public'))
        .pipe(rev())
        .pipe(gulp.dest('assets/public'))
        .pipe(rev.manifest())
        .pipe(revDel({dest: 'assets/public'}))
        .pipe(gulp.dest('assets/public'));
});

gulp.task('startwatch', function() {
    gulp.watch(config.paths.styles, function(e) {
        runSequence(['styles'], ['version']);
    });
    gulp.watch(config.paths.footerScripts, function(e) {
        runSequence(['scripts:header', 'scripts:footer', 'scripts:minOnly'], ['version']);
    });
});

gulp.task('refresh', function() {
    livereload.listen();
});

gulp.task('default', function() {
    runSequence(
        'clean',
        'lint',
        [
            'scripts:header',
            'scripts:footer',
            'scripts:minOnly',
            'styles'
        ],
        [
            'version',
            'startwatch'
        ],
        'refresh'
    );
});