<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
 <meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicon.png" type="image/png">
<script src="https://use.typekit.net/xqz3rum.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
 
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php
    /**
     * Functions hooked into storefront_before_header action
     *
     * @hooked storefront_secondary_navigation             - 30
     * @hooked storefront_product_search                   - 40
     */
	do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked into storefront_header action
			 *
			 * @hooked storefront_skip_links                       - 0
			 * @hooked storefront_social_icons                     - 10
			 * @hooked storefront_site_branding                    - 20
			 * @hooked storefront_primary_navigation_wrapper       - 42
			 * @hooked storefront_primary_navigation               - 50
			 * @hooked storefront_primary_navigation_wrapper_close - 68
			 */
			do_action( 'storefront_header' ); ?>

		</div>
	</header><!-- #masthead -->
	<?php 
	$user_d= wp_get_current_user();
	$role_d = array (
        'administrator',
		'dealer-1',
		'retailer-lad',
	);
	
	$role_chk = $user_d->roles[0];
	if ( $role_chk == 'dealer-1' ) 
	{ 
		$portal_url ="/lad-portal-home";
		$portal_name = "DEALER PORTAL";
	}
	else if ( $role_chk == 'retailer-lad' ) 
	{ 
		$portal_url ="/lad-portal-home";
		$portal_name = "DEALER PORTAL";
	}
	else
	{
		$portal_url ="/home";
	}   
	if(array_intersect( $role_d, $user_d->roles)){
	?>
	<div class="emilyHeader" style="margin:auto;padding:40px 40px !important;">
            <div class="emily-heading"><a href="<?php echo $portal_url;?>" style="color:#565656;"><?php echo $portal_name; ?></a> </div>
            <div class="emily-user">
            <p><i class="fa fa-user" aria-hidden="true"></i>Hello <strong><?php echo do_shortcode('[username_code]');?></strong> <a class="viewacc" href="/my-account/">View Account</a></p>
            </div>
        </div>
    
	<?php } ?>
	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' );
