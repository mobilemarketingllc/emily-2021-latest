<?php
/**
 * The template for displaying the Marketing Materials.
 *
 * Template name: LAD Marketing Material
 *
 * @package storefront
 */
get_header(); ?>
<style>
#portfoliolist .portfolio {
    margin: 2% 1% !important;
}

</style>
<link rel="stylesheet" href="<?php echo  bloginfo('stylesheet_directory'); ?>/assets/js/layout.css">
<script type="text/javascript" src="<?php echo  bloginfo('stylesheet_directory'); ?>/assets/js/jquery.mixitup.min.js"></script>
	<div id="primary" class="content-area">
		<main id="main" class="site-main home-site-main marketingMaterial" role="main">
      
        <div class="pageTitleBackBtn">
            <div class="backBtn"><a href="/lad-portal-home/">Back</a></div>
            <h1>ARCHITECTURAL SERIES PORTAL</h1>
        </div>
<script type="text/javascript">
    jQuery(function() {
        var filterList = {
            init: function() {
                // MixItUp plugin
                // http://mixitup.io
                jQuery('#portfoliolist').mixItUp({
                    selectors: {
                        target: '.portfolio',
                        filter: '.filter'
                    },
                    load: {
                        filter: '.images'
                    }
                });
            }
        };
        // Run the show!
        filterList.init();
    });
</script>
<ul id="filters" class="clearfix">
<?php 
$terms = get_terms( array(
	'taxonomy' => 'lad_marketing_cat',
	'hide_empty' => false,  ) ); 
foreach($terms as $term){
?>
   <li><span class="filter" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></span></li>  
<?php } ?>
</ul>
<div id="portfoliolist">
	<?php 
	    $args = array( 'post_type' => 'lad_market_material','posts_per_page'=> -1,'orderby' => 'title',	'order'   => 'ASC', );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();				
        $term_obj_list = get_the_terms( $post->ID, 'lad_marketing_cat' );	
        if($term_obj_list[0]->slug != 'images'){
            $class_img = "imagenot";
        }
        else
        {
            $class_img = "";
        }	
	?>
    <div class="portfolio <?php echo $term_obj_list[0]->slug?> <?php echo $class_img; ?>" data-cat="<?php echo $term_obj_list[0]->slug;?>">
        <div class="portfolio-wrapper">
        <?php  if($term_obj_list[0]->slug == 'images') { ?>   
        <div class="portfolio-imgWrapper">
         <a href="<?php the_permalink();?>"><img src="<?php the_post_thumbnail_url(array(300, 300)); ?>" alt="" /></a>  
        </div>			 			
        <div class="label">
            <div class="label-text">
				<a href="<?php the_permalink();?>" class="text-title"><?php the_title(); ?></a>           
            </div>
            <div class="label-bg"></div>
        </div>
        <?php } else { ?>
            <ul class="material_rooms imgDownloadPage">
            <?php while( have_rows('material_gallery') ): the_row(); 
                $image = get_sub_field('upload_image');
                $content = get_sub_field('image_title');
                $download = get_sub_field('download_file');
            ?>
            <li class="portfolio-imgWrapper"> 
            <?php  if($term_obj_list[0]->slug == 'videos') { ?>    
                <video width="300" poster="<?php echo $image; ?>" controls>
                <source src="<?php echo $download; ?>" type="video/mp4">                 
                Your browser does not support the video tag.
                </video>
                <a href="<?php echo $download; ?>" download>
                    <span class="mat_title"><?php echo $content; ?></span>
                    <img src="/wp-content/uploads/2018/12/download.png" width="24" style="margin: 0px !important;" />
                </a>
            <?php } else { ?> 
                <img src="<?php echo $image; ?>" alt="<?php echo $content; ?>" width="300" />
                <a href="<?php echo $download; ?>" download>
                    <span class="mat_title"><?php echo $content; ?></span>
                    <img src="/wp-content/uploads/2018/12/download.png" width="24" style="margin: 0px !important;" />
                </a>
                <?php } ?>
            </li>
            <?php endwhile; ?>
            </ul>         
            <div class="clearfix"></div>
        <?php } ?>    
        </div>
	</div>
<?php endwhile; ?>
    <div class="clearfix"></div>
</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer(); 