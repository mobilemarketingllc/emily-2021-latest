<?php
/**
 * The template used for displaying page content in page-home.php
 *
 * @package storefront
 */

$post_id = get_the_ID();

// Featured content slides (if applicable)
$featured_content_slides = get_post_meta($post_id, "emh_featured_content_slides", true);

// Featured product information
$featured_products_headline = get_post_meta($post_id, "emh_featured_products_headline", true);
$featured_products_desc = get_post_meta($post_id, "emh_featured_products_desc", true);

// Collections information
$collection_terms = get_terms(array(
    "taxonomy" => "collections"
));
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php /*if (!empty($featured_content_slides)) { ?>
    <div class="featured-content">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach($featured_content_slides as $slide) {
                    $bg = $slide["slide_background"];
                    $headline = $slide["slide_headline"];
                    $subheadline = $slide["slide_subheadline"];
                    $cta = $slide["slide_cta"];
                    $cta_url = $slide["slide_cta_url"]; ?>
                    <div class="swiper-slide" style="background-image: url(<?php echo $bg; ?>)">
                        <?php if (!empty($headline)) { ?>
                        <h2><?php echo htmlentities($headline); ?></h2> 
                        <?php }
                        if (!empty($subheadline)) { ?>
                        <h3><?php echo htmlentities($subheadline); ?></h3>
                        <?php } 
                        if (!empty($cta)) { ?>
                        <div class="featured-content-cta-container">
                            <a class="featured-content-cta" href="<?php echo $cta_url; ?>"><?php echo htmlentities($cta); ?></a>
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button swiper-button-prev"><span class="fa fa-chevron-left" /></div>
            <div class="swiper-button swiper-button-next"><span class="fa fa-chevron-right" /></div>
        </div>
    </div>
    <?php }*/ ?>

    
    <div class="containerMain">
        <?php the_content(); ?>
        <?php //echo apply_filters("the_content", $featured_products_desc); ?>
    </div>

    <div class="featured-products">
        <h2><?php echo htmlentities($featured_products_headline); ?></h2>
        
        <div id="featured-products-tabs">
            <ul>
                <li><a href="#flooring">Flooring</a></li>
                <li><a href="#furniture">Furniture</a></li>
                <li><a href="#home-decor">Home Décor</a></li>
            </ul>
            <div id="flooring">
                <?php featured_products('flooring'); ?>
            </div>
            <div id="furniture">
                <?php featured_products('furniture'); ?>
            </div>
            <div id="home-decor">
                <?php featured_products('home-decor'); ?>
            </div>
        </div>
    </div>
    <div class="entry-content">
        <?php //the_content(); ?>
    </div><!-- .entry-content -->

	<?php
	/**
	 * Functions hooked in to storefront_page add_action
	 *
	 * @hooked storefront_init_structured_data - 30
	 */
    
    // Suppress the default header
    remove_action('storefront_page', 'storefront_page_header', 10);

    // Suppress the default content render
    remove_action('storefront_page', 'storefront_page_content', 20);

    // Render the home page
	do_action('storefront_page');
	?>
</div><!-- #post-## -->
<script>
    jQuery(function() {
        if (window.innerWidth >= 768) {
            jQuery("#featured-products-tabs").tabs();
        }
    });
</script>

<?php
function featured_products($slug) {
    for ($idx = 1; $idx <= 6; $idx++) {
        $option = 'emh_featured_products_' . $slug . '_' . $idx;
        $product_id = cmb2_get_option('admin_featured_products', $option);
        $featured_product = get_post($product_id);
        $featured_product_image = get_the_post_thumbnail_url($featured_product->ID, array(300, 300));
        $title = emh_product_title($featured_product);
        $desc = emh_product_description($featured_product);
        $tile_class = "featured-product-tile";
        if ($idx % 3 == 0) {
            $tile_class .= " featured-product-tile-last";
        }
        ?>
        <a class="<?php echo $tile_class; ?>" href="<?php echo get_permalink($featured_product->ID); ?>" style="background-image: url('<?php echo $featured_product_image; ?>');">
            <span class="featured-product-tile-title"><?php echo htmlentities($title); ?>
                <span class="featured-product-tile-desc"><p><?php echo $desc; //apply_filters("the_content", $desc); ?></p></span>
            </span>
        </a>
        <?php
    }
}