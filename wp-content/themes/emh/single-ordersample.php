<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/woocommerce/assets/js/zoom/jquery.zoom.min.js?ver=1.7.15'></script>
<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/woocommerce/assets/js/flexslider/jquery.flexslider.min.js?ver=2.6.1'></script>
<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1'></script>
<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=3.1.2'></script>
<style>
.single-product .price,
.single-product .wc-measurement-price-calculator-price,
.single-product #price_calculator,
.single-product .quantity,
.single-product .single_add_to_cart_button,
.single-product .productinfo-show-discounts{
    display:block !important;
} 


</style>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home-site-main" role="main">
		
			<?php 
			$post_sample_box = get_field('product_sample_box');
			/**
			* Hide sample box product based on user role and category.
			*/

			if(is_user_logged_in() && $role[0]=='dealer-1'){

				echo do_shortcode('[product_page id="'.$post_sample_box.'"]');
			}
			$post_sample_carton = get_field('product_sample_carton');

			echo do_shortcode('[product_page id="'.$post_sample_carton.'"]');
			
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();